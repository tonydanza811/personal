#!/usr/bin/env python 
# -*- coding: utf-8 -*- 

import sys
import requests
import re
import time
from bs4 import BeautifulSoup


class GroupParser(object):
    """
    parses name, alias and ID of group subscribers
    """

    def __init__(self, bots, params, result_model):
        self.session = bots[0].session
        self.params = params or {} #group_id
        self.results = result_model


    def run(self):
        self.results.insert_many(self.parse_server_response(**self.params))
        return True


    def get_server_response(self, group_id):
        """
        returns server response
        """
        url = 'http://vk.com/al_page.php'
        post_data = {'act': 'box',
                    'al': '1',
                    #'oid': '-75895219',
                    'oid': '-' + group_id,
                    'tab': 'members'}
        r = self.session.post(url, data = post_data)
        return r.content.decode('cp1251').replace('\n', '')


    def get_maximum_offset(self, group_id):
        """
        returns number of people found by query
        """
        data = self.get_server_response(group_id).split('<!>')
        soup = BeautifulSoup(data[6])
        subs_number_list = soup.find('span', class_='fans_count').contents
        res_number = [elem for elem in subs_number_list if isinstance(elem, str)]
        return int(''.join(res_number))


    def parse_users(self, data):
        """
        data - group of 'fans_fan_lnk' clases
        
        returns list of dictionaries {'name': name, 'href': href, 'id': id} for one offset
        """
        result = []
        for block in data:
            wrap = block.find('div', class_='fans_fanph_wrap')
            if wrap.has_attr('onmouseover'):
                user_id = wrap['onmouseover'].split()[1][:-1]
            else:
                user_id = ''
            user_name = block.find('a', class_='fans_fan_lnk')
            result.append({'name': user_name.string,
                        'id': user_id,
                        'alias': user_name['href'][1:]})
        return result


    def parse_server_response(self, group_id):
        """
        returns list of dictionaries {'name': name, 'href': href, 'id': id}
        """
        #f = open('true_server_response.txt', 'a')
        result = []
        self.post_data['offset'] = 0
        offset = 0
        max_offset = self.get_maximum_offset(group_id)
        print('Maximum offset is: ' + str(max_offset))

        while offset < max_offset:
            self.post_data['offset'] = offset
            print('Offset is: ' + str(offset))
            inf = self.get_server_response(group_id).split('<!>')
            data = inf[5]
            #f.write(data)
            soup = BeautifulSoup(data)
            users_data = soup.findAll('div', class_='fans_fan_row inl_bl')
            result.extend(self.parse_users(users_data))
            if offset == 0:
                offset += 120
            else:
                offset += 60 
            time.sleep(0.5)

        # count = 0
        # for elem in result:
        #     count += 1
        # print(str(count) + ' people found')
        return result


    @staticmethod
    def get_info():
        """
        Returns all parameters of parser with their descriptions and titles
        """
        return {
            'id': 'group_subscribers',
            'num_bots': 1,
            'info': {
                'title': 'Group subscribers scrambler',
                'description': 'Gets users aliases and links'
            },
            'params': {
                'group_id': {
                    'description': 'ID of Group to scramble info from',
                    'title': 'Group ID',
                }
            }
        }